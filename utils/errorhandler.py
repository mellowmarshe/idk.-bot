import traceback
import sys
from discord.ext import commands
import discord

class CommandErrorHandler:
    def __init__(self, bot):
        self.bot = bot

    async def on_command_error(self, ctx, error):
        if hasattr(ctx.command, 'on_error'):
            return
        
        ignored = (commands.MissingRequiredArgument, commands.BadArgument, commands.NoPrivateMessage, commands.CheckFailure, commands.DisabledCommand, commands.CommandInvokeError, commands.TooManyArguments, commands.UserInputError, commands.NotOwner, commands.MissingPermissions, commands.BotMissingPermissions)   
        error = getattr(error, 'original', error)
        
        if isinstance(error, ignored):
            embed = discord.Embed(color=0x000000, title=' ', description=
f'''
    An error has occured! {error}
''')
            await ctx.send(embed=embed)   
        if isinstance(error, commands.CommandNotFound):
            await ctx.message.add_reaction("❓")
        else:
            print (f'{error}')

def setup(bot):
    bot.add_cog(CommandErrorHandler(bot))