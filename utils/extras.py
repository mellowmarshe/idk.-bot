FANCY_CHARS = {
    "A": "Ａ", "B": "Ｂ", "C": "Ｃ", "D": "Ｄ", "E": "Ｅ",
    "F": "Ｆ", "G": "Ｇ", "H": "Ｈ", "I": "Ｉ", "J": "Ｊ",
    "K": "Ｋ", "L": "Ｌ", "M": "Ｍ", "N": "Ｎ", "O": "Ｏ",
    "P": "Ｐ", "Q": "Ｑ", "R": "Ｒ", "S": "Ｓ", "T": "Ｔ",
    "U": "Ｕ", "V": "Ｖ", "W": "Ｗ", "X": "Ｘ", "Y": "Ｙ",
    "Z": "Ｚ", ".": "．", "!": "！", "?": "？", "1": "１",
    "2": "２", "3": "３", "4": "４", "5": "５", "6": "６",
    "7": "７", "8": "８", "9": "９", "0": "０", "(": "（",
    ")": "）"
}

HUG_RESPONSES = ['''$giver hugged $receiver... when's the wedding?''', '''$giver hugged $receiver!''', '''This is the cutest thing ever... $giver hugging $receiver''',
                '''Hugs for all!''', '''Wow $receiver hugged by $giver now where's my hug?''']

PAT_RESPONSES = ['''$giver pat $receiver! owo''', '''$giver pat $receiver! UwU''', '''I want pats too. :(''']

POKE_RESPONSES = ['''Pokety poke poke... $giver just poked $receiver!''', '''$giver poked $receiver''', '''No pats for me? Fine!''']

EMBED_COLORS = [0xDF2540, 0xE9627C, 0xEF694F, 0xF36B7F, 0xF32929]
