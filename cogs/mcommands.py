import discord
from discord.ext import commands
from discord.ext.commands import cooldown
from discord.ext.commands.cooldowns import BucketType
import asyncio
from datetime import datetime
import time

class MemberID(commands.Converter):
    async def convert(self, ctx, argument):
        try:
            m = await commands.MemberConverter().convert(ctx, argument)
        except commands.BadArgument:
            try:
                return int(argument, base=10)
            except ValueError:
                raise commands.BadArgument(f"{argument} invalid member or ID.") from None
        else:
            can_execute = ctx.author.id == ctx.bot.owner_id or \
                          ctx.author == ctx.guild.owner or \
                          ctx.author.top_role > m.top_role

            if not can_execute:
                raise commands.BadArgument('You cannot perform this action due to role hierarchy.')
            return m.id

class BannedMember(commands.Converter):
    async def convert(self, ctx, argument):
        ban_list = await ctx.guild.bans()
        try:
            member_id = int(argument, base=10)
            entity = discord.utils.find(lambda u: u.user.id == member_id, ban_list)
        except ValueError:
            entity = discord.utils.find(lambda u: str(u.user) == argument, ban_list)
        if entity is None:
            raise commands.BadArgument("Not a valid previously-banned member.")
        return entity

class mcommands():
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.guild_only()
    async def ban(self, ctx, user: MemberID, *, reason=None):
        if reason is None:
            reason = 'NO REASON'
        await ctx.guild.ban(discord.Object(id=user), reason=f'''{user} was banned for {reason} by {ctx.author}''')
        await ctx.send(f'{ctx.author} banned {user} for {reason}')

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(kick_members=True)
    async def softban(self, ctx, user: discord.Member, *, reason=None):
        if reason is None:
            reason = f'NO REASON'
        await ctx.guild.ban(user, reason=f'{user} was banned for {reason} by {ctx.author}')
        await ctx.guild.unban(user, reason=f'{user} was banned for {reason} by {ctx.author}')
        await ctx.send(f'{ctx.author} softbanned {user} for {reason}')

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(ban_members=True)
    async def unban(self, ctx, user: BannedMember, *, reason=None):          
        if reason is None:
            reason = f'NO REASON'
        await ctx.guild.unban(user.user, reason=reason)
        await ctx.send(f'{ctx.author} unbanned {user.user} for {reason}.')

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(kick_messages=True)
    async def kick(self, ctx, user: discord.Member, *, reason=None):
        if user is None:
            await ctx.send('Please specify the user you would like to kick.')
        if reason is None:
            reason = 'NO REASON'
        await ctx.guild.kick(user, reason=f'{user} was banned for {reason} by {ctx.author}')
        await ctx.send(f'{ctx.author} just kicked {user} for {reason}.')

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(manage_messages=True)
    async def clear(self, ctx, num:int, target:discord.Member=None):
        if num > 500 or num == 0:
            await ctx.send ('Invalid amount. Can clear a max of 500 messages per use.')
            return
        def msgcheck(amsg):
            if target:
                return amsg.author.id==target.id
            return True
        await ctx.channel.purge(limit=num, check=msgcheck)
        await ctx.send (f'Deleted {num} messages.')

    @commands.command()
    @commands.guild_only()
    async def id(self, ctx, user: discord.Member=None):
        if user is None:
            user = ctx.author
        await ctx.send(f'''`{user.name}`'s ID is `{user.id}`.''')

    @commands.command()
    @commands.has_permissions(manage_roles=True)
    async def mute(self, ctx, user: discord.Member, *, reason=None):
        """Mutes a user from the channel."""
        if reason is None:
            reason = 'NO REASON'
        try:
            await ctx.channel.set_permissions(user, send_messages=False, reason=f'{user} was banned for {reason} by {ctx.author}')
        except:
            await ctx.send("Failed to mute the user.")
        else:
            await ctx.send(f'Muted {user.name} for {reason}.')

    @commands.command()
    @commands.has_permissions(manage_roles=True)
    async def unmute(self, ctx, *, user: discord.Member):
        """Unmutes a user from the channel."""
        reason = 'NO REASON'
        try:
            await ctx.channel.set_permissions(user, send_messages=True, reason=f'{user} was banned for {reason} by {ctx.author}')
        except:
            await ctx.send("Failed to unmute the user.")
        else:
            await ctx.send(f'Unmuted {user.name}.')

def setup(bot):
	bot.add_cog(mcommands(bot))