import sys
import discord
from discord.ext import commands
from discord.ext.commands import cooldown
from discord.ext.commands.cooldowns import BucketType
import asyncio
from datetime import datetime
import time
from math import *
import psutil

class help():
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=['about', 'stats'])
    @commands.guild_only()
    async def info(self, ctx):
        data = '''SELECT serverprefix FROM guildprefix WHERE guildid = $1'''
        majorv = sys.version_info[0]
        minorv = sys.version_info[1]
        microv = sys.version_info[2]
        prefix = await self.bot.pool.fetchval(data, ctx.message.guild.id)
        if await self.bot.pool.fetchrow('''SELECT serverprefix FROM guildprefix WHERE guildid = $1;''', ctx.message.guild.id) is None:
            prefix = '!~'
        data = await self.bot.pool.fetchval('''SELECT * FROM cmdcount;''')
        embed = discord.Embed(color=0x000000, title=' ', description=
f'''
Commands used: {data}
Server prefix: {prefix}. You can also do @idk.#2412 to use commands
Made by mellowmarshe#0001

**[Invite](https://discordapp.com/api/oauth2/authorize?client_id=476570085582962718&permissions=8&scope=bot)**
**[Server](https://discord.gg/sedt4tY)**
**[Vote](https://discordbots.org/bot/476570085582962718/vote)**

**Host Stats:**
cpu: {psutil.cpu_percent()}%
memory: {psutil.virtual_memory()[2]}%
Python Version: {majorv}.{minorv}.{microv}
Discord.py Version: 1.0.0a
**Bot Stats:**
Guilds: {len(self.bot.guilds)}
Users: {len(self.bot.users)}
Ping: {ceil(self.bot.latency * 1000)}
''')
        embed.set_author(name='Bot Stats')
        await ctx.send(embed=embed)

    @commands.group(invoke_without_command=True)
    @commands.guild_only()
    async def help(self, ctx):
        majorv = sys.version_info[0]
        minorv = sys.version_info[1]
        microv = sys.version_info[2]
        data = '''SELECT serverprefix FROM guildprefix WHERE guildid = $1'''
        prefix = await self.bot.pool.fetchval(data, ctx.message.guild.id)
        if await self.bot.pool.fetchrow('''SELECT serverprefix FROM guildprefix WHERE guildid = $1;''', ctx.message.guild.id) is None:
            prefix = '!~' 
        e = discord.Embed(color=0x000000, title='', description=f'Made in Pyton {majorv}.{minorv}.{microv} using discord.py version 1.0.0a. Made by mellowmarshe#0001. Use `help <category>` for help on a category.')
        e.add_field(name='Links', value=
'''  
**[Invite](https://discordapp.com/api/oauth2/authorize?client_id=476570085582962718&permissions=8&scope=bot)** | **[Server](https://discord.gg/sedt4tY)** | **[Vote](https://discordbots.org/bot/476570085582962718/vote)**
''')
        e.add_field(name='Prefix', value=
f'''  
Prefixes: `{prefix}` and `@idk.#2412`
''')
        e.add_field(name='Autorole Commands. 3 commands', value=
'''   
`autorole set, autorole disable, autorole`
''')
        e.add_field(name='Modlog. 4 commands', value=
'''   
`modlogs, modlogs channel, modlogs disable, modlogs toggle`
''')
        e.add_field(name='Prefix. 3 commands', value=
'''   
`prefix, prefix set, prefix reset`
''')
        e.add_field(name='Moderation. 8 commands', value=
'''   
`ban, softban, unban, kick, clear, id, mute, unmute`
''')
        e.add_field(name='Fun. 11 commands', value=
'''   
`pat, hug, poke, avatar, clap, reverse, wonky, osu, aesthetic, cowsay, echo`
''')
        e.add_field(name='General. 5 commands', value=
'''   
`ping, uptime, info, vote, support`
''')
        e.set_author(name='Help')
        await ctx.send(embed=e)

    @help.command()
    @commands.guild_only()
    async def prefix(self, ctx):       
        e = discord.Embed(color=0x000000, title='', description=
f'''            
<> required [] optional. All prefix commands requires administrator.
Custom server prefixes replace `!~`. 
''')
        e.add_field(name='Links', value=
'''  
**[Invite](https://discordapp.com/api/oauth2/authorize?client_id=476570085582962718&permissions=8&scope=bot)** | **[Server](https://discord.gg/sedt4tY)** | **[Vote](https://discordbots.org/bot/476570085582962718/vote)**
''')
        e.add_field(name='prefix', value=
'''
Tells current server prefix. Does not administrator.
''', inline=False)
        e.add_field(name='prefix set', value=
'''
To set a custom server prefix you use `prefix set <prefix>`. The prefix is not allowed to be more then 5 characters long.
''', inline=False)
        e.add_field(name='prefix reset', value=
'''
Resets server prefix to `!~`.
''', inline=False)
        e.set_author(name='Prefix Commands')
        await ctx.send(embed=e)

    @help.command()
    @commands.guild_only()
    async def autorole(self, ctx):    
        e = discord.Embed(color=0x000000, title='', description=
f'''            
<> required [] optional. All autorole commands requires manage roles.
Autorole gives every member that joins the role specified.
''')
        e.add_field(name='Links', value=
'''  
**[Invite](https://discordapp.com/api/oauth2/authorize?client_id=476570085582962718&permissions=8&scope=bot)** | **[Server](https://discord.gg/sedt4tY)** | **[Vote](https://discordbots.org/bot/476570085582962718/vote)**
''')
        e.add_field(name='autorole', value=
'''
Tells current autorole.
''', inline=False)
        e.add_field(name='autorole set <role>', value=
'''
Sets a role to the autorole.
''', inline=False)
        e.add_field(name='autorole disable', value=
'''
Disables autorole
''', inline=False)
        e.set_author(name='Autorole Commands')
        await ctx.send(embed=e)

    @help.command()
    @commands.guild_only()
    async def modlogs(self, ctx):    
        e = discord.Embed(color=0x000000, title='', description=
f'''            
<> required [] optional. All modlogs commands requires manage channel.
Modlogs is basically the audit log but way better with logs like: Message Delete, Message Update!
''')
        e.add_field(name='Links', value=
'''  
**[Invite](https://discordapp.com/api/oauth2/authorize?client_id=476570085582962718&permissions=8&scope=bot)** | **[Server](https://discord.gg/sedt4tY)** | **[Vote](https://discordbots.org/bot/476570085582962718/vote)**
''')
        e.add_field(name='modlogs', value =
'''
Tells modlog channel.
''', inline=False)
        e.add_field(name='modlogs channel [channel]', value=
'''
Sets mod-log channel. If no channel specified sets modlog channel to channel where command is invoked.
''', inline=False)
        e.add_field(name='modlogs disable', value=
'''
Disables modlogs.
''', inline=False)
        e.add_field(name='modlogs toggle <type>', value=
'''
Toggles modlog. Modlog types are delmsg, editmsg, chnnelcreate, chnneldelete, memjoin, memleave, rolecreate, roledelete, memban, and memunban.
''', inline=False)
        e.set_author(name='Modlog Commands')
        await ctx.send(embed=e)

    @help.command()
    @commands.guild_only()
    async def moderation(self, ctx):     
        e = discord.Embed(color=0x000000, title='', description=
f'''            
<> required [] optional. 
Moderation commands like softban and ban!
''')
        e.add_field(name='Links', value=
'''  
**[Invite](https://discordapp.com/api/oauth2/authorize?client_id=476570085582962718&permissions=8&scope=bot)** | **[Server](https://discord.gg/sedt4tY)** | **[Vote](https://discordbots.org/bot/476570085582962718/vote)**
''')
        e.add_field(name='ban <userID> [reason]', value=
'''
Bans a user by there ID. Can ban users who are not in the server. If no reason specified reason is NO REASON.
Required Permissions: Ban Members
''', inline=False)
        e.add_field(name='softban <user> [reason]', value=
'''
Bans then unbans a user basically clearing all there messages and kicking the user. If no reason specified reason is NO REASON.
Required Permissions: Kick Members
''', inline=False)
        e.add_field(name='unban <userID> [reason]', value=
'''
Unbans a user by there ID. If no reason is specified reason is NO REASON.
Required Permissions: Ban Members
''', inline=False)
        e.add_field(name='kick <user> [reason]', value=
'''
Kicks the user. If no reason specified reason is NO REASON.
Required Permissions: Kick Members
''', inline=False)
        e.add_field(name='clear <amount> [user]', value=
'''
Clears X amount of messages max of 500 per use. If user is specified deletes X amount of messages from that user.
Required Permissions: Manage Messages
''', inline=False)
        e.add_field(name='id <user>', value=
'''
Gets a usersid or your id.
''', inline=False)
        e.add_field(name='addrole <user> <role>', value=
'''
Gives the user the role.
''', inline=False)
        e.add_field(name='mute <user> [reason]', value=
'''
Prevents a user from talking in a specific channel.
''', inline=False)
        e.add_field(name='unmute <user>', value=
'''
Allows the user to talk again.
''', inline=False)
        e.set_author(name='Moderation Commands')
        await ctx.send(embed=e)

    @help.command()
    @commands.guild_only()
    async def fun(self, ctx):      
        e = discord.Embed(color=0x000000, title='', description=
f'''            
<> required [] optional.
Fun commands like poke and hug!
''')
        e.add_field(name='Links', value=
'''  
**[Invite](https://discordapp.com/api/oauth2/authorize?client_id=476570085582962718&permissions=8&scope=bot)** | **[Server](https://discord.gg/sedt4tY)** | **[Vote](https://discordbots.org/bot/476570085582962718/vote)**
''')
        e.add_field(name='pat [user]', value=
'''
Pat pat pat.
''', inline=False)
        e.add_field(name='hug [user]', value=
'''
Come here and accept my hugs!
''', inline=False)
        e.add_field(name='poke [user]', value=
'''
Yes... yes... pokety poke.
''', inline=False)
        e.add_field(name='avatar [user]', value=
'''
Shows a users or your avatar.
''', inline=False)
        e.add_field(name='clap [text]', value=
'''
Claps :clap: a :clap: message.
''', inline=False)
        e.add_field(name='reverse [text]', value=
'''
.txet eht sesreveR
''', inline=False)
        e.add_field(name='wonky [text]', value=
'''
wONKIFIES TEXT.
''', inline=False)
        e.add_field(name='osu <user>', value=
'''
Gets the users osu! stats.
''', inline=False)
        e.add_field(name='aesthetic [text]', value=
'''
Ａ Ｅ Ｓ Ｔ Ｈ Ｅ Ｔ Ｉ Ｃ．
''', inline=False)
        e.add_field(name='cowsay [text]', value=
'''
Makes the cow say something.
''', inline=False)
        e.add_field(name='echo [text]', value=
'''
Makes the bot say the message.
''', inline=False)
        e.set_author(name='Fun Commands')
        await ctx.send(embed=e)

    @help.command()
    @commands.guild_only()
    async def general(self, ctx):      
        e = discord.Embed(color=0x000000, title='', description=
f'''            
<> required [] optional.
General commands.
''')
        e.add_field(name='Links', value=
'''  
**[Invite](https://discordapp.com/api/oauth2/authorize?client_id=476570085582962718&permissions=8&scope=bot)** | **[Server](https://discord.gg/sedt4tY)** | **[Vote](https://discordbots.org/bot/476570085582962718/vote)**
''')
        e.add_field(name='ping', value=
'''
Sends bots ping.
''', inline=False)
        e.add_field(name='uptime', value=
'''
Gets the bots uptime.
''', inline=False)
        e.add_field(name='info', value=
'''
Shows general stats about the bot
''', inline=False)
        e.add_field(name='vote', value=
'''
Gets a link so you can upvote the bot. Upvoting the bot is a good free way of supporting the bot.
Alias: upvote
''', inline=False)
        e.add_field(name='support', value=
'''
Sends support server invite.
Alias: invite
''', inline=False)
        e.set_author(name='General Commands')
        await ctx.send(embed=e) 

def setup(bot):
	bot.add_cog(help(bot))