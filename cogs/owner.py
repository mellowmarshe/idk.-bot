import discord
from discord.ext import commands
import asyncpg
import time
from functools import *
from discord.ext.commands.cooldowns import BucketType
import ast

class owner():
    def __init__(self, bot):
        self.bot = bot

    @commands.command(hidden=True)
    @commands.guild_only()
    async def eval(self, ctx, *, body: str):
        """Evaluates a code"""
        if ctx.message.author.id == 300088143422685185:
            fn_name = "_eval_expr"
            cmd = "\n".join(f"    {i}" for i in body.splitlines())
            body = (f"async def {fn_name}():\n"+ cmd)
            parsed = ast.parse(body)
            body = parsed.body[0].body
            if isinstance(body[-1], ast.Expr):
                body[-1] = ast.Return(body[-1].value)
            ast.fix_missing_locations(body[-1])
            env = {
                'bot': ctx.bot,
                'discord': discord,
                'commands': commands,
                'ctx': ctx,
                'import': __import__
            }
            exec(compile(parsed, filename="<ast>", mode="exec"), env)
            returned = await eval("{}()".format(fn_name), env)    
            await ctx.send("```{}```".format(returned))  
        else:
            pass

def setup(bot):
    bot.add_cog(owner(bot))