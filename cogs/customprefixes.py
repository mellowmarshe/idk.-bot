import discord
from discord.ext import commands
from discord.ext.commands import cooldown
from discord.ext.commands.cooldowns import BucketType
import asyncio
import asyncpg

class customprefixes():
    def __init__(self, bot):
        self.bot = bot

    @commands.group(invoke_without_command=True)
    @commands.guild_only()
    async def prefix(self, ctx):
        if await self.bot.pool.fetchrow('''SELECT guildid FROM guildprefix WHERE guildid = $1;''', ctx.message.guild.id) is None:
            await ctx.send('This server does not have a custom prefix. Use `!~` to invoke commands.')
        else:
            data = '''SELECT * FROM guildprefix WHERE guildid = $1'''
            row = await self.bot.pool.fetchrow(data, ctx.message.guild.id)
            await ctx.send(f'''This servers custom prefixes `{row['serverprefix']}`. Use `{row['serverprefix']}` to invoke commands. ''')

    @prefix.command()
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def set(self, ctx, *, prefix: commands.clean_content(fix_channel_mentions=False, use_nicknames=False, escape_markdown=True)):
        if len(prefix) >= 75:
            await ctx.send('Invalid prefix. Prefix can not be more then 75 charcters in length.')
        if await self.bot.pool.fetchrow('''SELECT guildid FROM guildprefix WHERE guildid = $1;''', ctx.message.guild.id) is None:
            await  self.bot.pool.execute('''INSERT INTO guildprefix (guildid, serverprefix) VALUES ($1, $2);''', ctx.message.guild.id, prefix)
        else:
            await self.bot.pool.execute('''UPDATE guildprefix SET serverprefix = $1 WHERE guildid = $2;''', prefix, ctx.message.guild.id)
        await ctx.send(f'The new server prefix is `{prefix}`.')

    @prefix.command()
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def reset(self, ctx):
        if await self.bot.pool.fetchrow('''SELECT guildid FROM guildprefix WHERE guildid = $1;''', ctx.message.guild.id) is None:
            await ctx.send('This server does not have a custom prefix. To add one use `!~prefix set <prefix>` prefix being the desired prefix.')
        else:
            await self.bot.pool.execute(f'''DELETE FROM guildprefix WHERE guildid = $1;''', ctx.message.guild.id)
            await ctx.send('Custom prefix now removed to set a new one use `!~prefix set <prefix>` prefix being the desired prefix.')
            
def setup(bot):
	bot.add_cog(customprefixes(bot))