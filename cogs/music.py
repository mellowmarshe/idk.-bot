import discord
from discord.ext import commands
import asyncio
import itertools, datetime
import sys
import traceback
from async_timeout import timeout
from functools import partial
from youtube_dl import YoutubeDL
from discord.ext.commands.cooldowns import BucketType

class music:
    def __init__(self, bot):
        self.bot = bot

def setup(bot):
    bot.add_cog(music(bot))