import discord
from discord.ext import commands
from discord.ext.commands import cooldown
from discord.ext.commands.cooldowns import BucketType
import asyncio
from datetime import datetime
import time
 
class rolemanagement():
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(manage_roles=True)
    async def addrole(self, ctx, user: discord.Member=None, *, roles: discord.Role=None):
        if ctx.author.bot is False:
            if roles is None:
                await ctx.send('No role(s) specified.')
            if user is None:
                await ctx.send('No user specified.')
            else:
                await user.add_roles(roles)
                await ctx.send(f'Gave {user.name} {roles} role(s).')
        else:
            pass

    @commands.group(invoke_without_command=True)
    @commands.guild_only()
    async def autorole(self, ctx):
        if ctx.author.bot is False:
            if await self.bot.pool.fetchrow('''SELECT guildid FROM autorole WHERE guildid = $1;''', ctx.message.guild.id) is None:
                await ctx.send('This server does not have autorole setup. Use `autorole set <role>` to set the autorole.')
            else:
                query = '''SELECT * FROM autorole WHERE guildid = $1'''
                row = await self.bot.pool.fetchrow(query, ctx.guild.id)  
                role = discord.utils.get(ctx.guild.roles, id=row['roleid'])
                await ctx.send(f'''This servers autorole is `{role.name}({role.id})`''')
        else:
            pass

    @autorole.command()
    @commands.guild_only()
    @commands.has_permissions(manage_roles=True)   
    async def set(self, ctx, *, role: discord.Role=None):
        if ctx.author.bot is False:
            if role is None:
                await ctx.send('Please specify which role you would like the autorole to be.')
            else:
                if await self.bot.pool.fetchrow('''SELECT guildid FROM autorole WHERE guildid = $1;''', ctx.message.guild.id) is None:
                    await  self.bot.pool.execute('''INSERT INTO autorole (guildid, roleid) VALUES ($1, $2);''', ctx.message.guild.id, role.id)
                else:
                    await self.bot.pool.execute('''UPDATE autorole SET roleid = $1 WHERE guildid = $2;''', role.id, ctx.message.guild.id)
                await ctx.send(f'This servers autorole was set to `{role.name}({role.id})`.')            
        else:
            pass

    @autorole.command()
    @commands.guild_only()
    @commands.has_permissions(manage_roles=True)   
    async def disable(self, ctx):
        if ctx.author.bot is False:
            if await self.bot.pool.fetchrow('''SELECT guildid FROM autorole WHERE guildid = $1;''', ctx.message.guild.id) is None:
                await ctx.send('This server does not have autorole setup. Use `autorole set <role>` to set the autorole.')     
            else:
                await self.bot.pool.execute('''DELETE FROM autorole WHERE guildid = $1''', ctx.guild.id)
                await ctx.send('Disabled autorole.')
        else:
            pass

    async def on_member_join(self, member):
        if await self.bot.pool.fetchrow('''SELECT guildid FROM autorole WHERE guildid = $1;''', member.guild.id) is None:        
            pass
        else:
            query = '''SELECT * FROM autorole WHERE guildid = $1;'''
            row = await self.bot.pool.fetchrow(query, member.guild.id)  
            role = discord.utils.get(member.guild.roles, id=row['roleid'])
            query = '''SELECT * FROM autorole WHERE guildid = $1;'''
            await member.add_roles(role)

def setup(bot):
	bot.add_cog(rolemanagement(bot))