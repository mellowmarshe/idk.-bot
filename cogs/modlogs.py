import discord
from discord.ext import commands
from discord.ext.commands import cooldown
from discord.ext.commands.cooldowns import BucketType
import asyncio
import time
import asyncpg
from datetime import datetime
import time

class modlogs():
    def __init__(self, bot):
        self.bot = bot

    @commands.group(invoke_without_command=True)
    @commands.guild_only()
    @commands.has_permissions(manage_channels=True)
    async def modlogs(self, ctx):
        if await self.bot.pool.fetchrow('''SELECT * FROM modlogs WHERE guildid = $1;''', ctx.guild.id) is None:
            await ctx.send('This server does not have mod-logs setup.')
        else:
            data = '''SELECT * FROM modlogs WHERE guildid = $1'''
            row = await self.bot.pool.fetchrow(data, ctx.guild.id)
            embed = discord.Embed(color=0x000000, title=' ', description=
f''' 
Message Delete: `{row['delmsg']}`
Message Edit: `{row['editmsg']}`
Channel Create: `{row['chnnelcreate']}`
Channel Delete: `{row['chnneldelete']}`
Channel Update: `{row['chnnelupdate']}`
Member Join: `{row['memjoin']}`
Member Leave: `{row['memleave']}`
Role Create: `{row['rolecreate']}`
Role Delete: `{row['roledelete']}`
Member Ban: `{row['memban']}`
Member Unban: `{row['memunban']}`
'''.replace('0', 'Enabled').replace('1', 'Disabled'))
            embed.set_author(name=f'Modlogs', icon_url=ctx.guild.icon_url)
            embed.add_field(name='Modlog Channel', value=f'''<#{row['modlogchan']}>({row['modlogchan']})''' )
            await ctx.send(embed=embed) 

    @modlogs.command()
    @commands.guild_only()
    @commands.has_permissions(manage_channels=True)
    async def channel(self, ctx, channel: discord.TextChannel=None):
        if channel is None:
            channel = ctx.message.channel
        if await self.bot.pool.fetchrow('''SELECT * FROM modlogs WHERE guildid = $1;''', ctx.guild.id) is None:
            await self.bot.pool.execute('''INSERT INTO modlogs (modlogchan, guildid) VALUES ($1, $2);''', channel.id, ctx.guild.id)
        else:
            await self.bot.pool.execute('''UPDATE modlogs SET modlogchan = $1 WHERE guildid = $2;''', channel.id, ctx.guild.id)
        await ctx.send(f'Set the mod-log channel to `{channel}`.')

    @modlogs.command()
    @commands.guild_only()
    @commands.has_permissions(manage_channels=True)
    async def disable(self, ctx, channel: discord.TextChannel=None):
        data = '''SELECT * FROM guildprefix WHERE guildid = $1'''
        row = await self.bot.pool.fetchrow(data, ctx.message.guild.id)
        if await self.bot.pool.fetchrow('''SELECT * FROM modlogs WHERE guildid = $1;''', ctx.guild.id) is None:
            await ctx.send(f'''This server does not have mod-logs setup. Use `{row['serverprefix']}modlogs channel <#channel>` to set the mod-log channel.''')
        else:
            await self.bot.pool.execute(f'''DELETE FROM modlogs WHERE guildid = $1;''', ctx.message.guild.id)
            await ctx.send('Mod-logs will no longer show.')

    @modlogs.command()
    @commands.guild_only()
    @commands.has_permissions(manage_channels=True)
    async def toggle(self, ctx, logtype=None):
        if await self.bot.pool.fetchrow('''SELECT * FROM modlogs WHERE guildid = $1;''', ctx.guild.id) is None:
            data = '''SELECT * FROM guildprefix WHERE guildid = $1'''
            row = await self.bot.pool.fetchrow(data, ctx.message.guild.id)
            await ctx.send(f'''This server does not have mod-logs setup. Use `{row['serverprefix']}modlogs channel <#channel>` to set the mod-log channel.''')
        if logtype is None:
            await ctx.send(f'''The available logtypes are `delmsg, editmsg, chnnelcreate, chnneldelete, memjoin, memleave, rolecreate, roledelete, memban, memunban`.''')
        if logtype == 'delmsg' or 'editmsg' or 'chnnelcreate' or 'chnneldelete' or 'memjoin' or 'memleave' or 'rolecreate' or 'roledelete' or 'roleupdate' or 'memban' or 'memunban':
            status = '0'
            if await self.bot.pool.fetchval(f'''SELECT {logtype} FROM modlogs WHERE guildid = $1;''', ctx.guild.id) is '0':
                status = '1'
            if await self.bot.pool.fetchrow('''SELECT guildid FROM modlogs WHERE guildid = $1;''', ctx.guild.id) is None:
                await self.bot.pool.execute(f'''INSERT INTO modlogs (guildid, {logtype}) VALUES ($1, $2);''', ctx.message.guild.id, status)
            else:
                await self.bot.pool.execute(f'''UPDATE modlogs SET {logtype} = $1 WHERE guildid = $2;''', status, ctx.message.guild.id)
            await ctx.send(f'`{logtype}`was set to `{status}`.'.replace('0', 'Enabled').replace('1', 'Disabled'))
        else:
            await ctx.send(f'''Invalid logtype: `{logtype}` the available logtypes are `delmsg, editmsg, chnnelcreate, chnneldelete, memjoin, memleave, rolecreate, roledelete, memban, memunban`.''')

    async def on_message_delete(self, message):
        if await self.bot.pool.fetchrow('''SELECT * FROM modlogs WHERE guildid = $1;''', message.guild.id) is None:
            pass
        if message.author.bot is True:
            pass
        else:
            if await self.bot.pool.fetchval(f'''SELECT delmsg FROM modlogs WHERE guildid = $1;''', message.guild.id) is '0':
                query = '''SELECT modlogchan FROM modlogs WHERE guildid = $1'''
                row = await self.bot.pool.fetchrow(query, message.guild.id)  
                channel = self.bot.get_channel(row['modlogchan'])
                #messagecon = await commands.clean_content.convert(message, f'''{message.content}''')
                await channel.send(
f'''
**Message Deleted**
`{message.author.name}`(`{message.author.id}`) in **{message.channel}**
**{message.clean_content}**
''')
            else:
                pass

    async def on_message_edit(self, before, after):
        if await self.bot.pool.fetchrow('''SELECT * FROM modlogs WHERE guildid = $1;''', before.guild.id) is None:
            pass
        if before.author.bot is True or after.author.bot is True:
            pass
        else:
            if await self.bot.pool.fetchval(f'''SELECT editmsg FROM modlogs WHERE guildid = $1;''', before.guild.id) is '0':
                query = '''SELECT modlogchan FROM modlogs WHERE guildid = $1'''
                row = await self.bot.pool.fetchrow(query, before.guild.id)  
                channel = self.bot.get_channel(row['modlogchan'])
                await channel.send(
f'''
**Message Update Log**
`{before.author.name}`(`{before.author.id}`) in **{before.channel}**
Before: **{before.clean_content}**
After: **{after.clean_content}**
''')
            else:
                pass

    async def on_guild_channel_create(self, channel):
        if await self.bot.pool.fetchrow('''SELECT * FROM modlogs WHERE guildid = $1;''', channel.guild.id) is None:
            pass
        else:
            if await self.bot.pool.fetchval(f'''SELECT chnnelcreate FROM modlogs WHERE guildid = $1;''', channel.guild.id) is '0':
                query = '''SELECT modlogchan FROM modlogs WHERE guildid = $1'''
                row = await self.bot.pool.fetchrow(query, channel.guild.id)  
                channels = self.bot.get_channel(row['modlogchan'])
                isnsfw = 'Not text channel'
                if type(channel) == discord.channel.TextChannel:
                    isnsfw = channel.is_nsfw()
                await channels.send(
f'''
**Channel Create Log**
`{channel.name}`(`{channel.id}`) category **{channel.category}**
NSFW: **{isnsfw}**
''')
            else:
                pass

    async def on_guild_channel_delete(self, channel):
        if await self.bot.pool.fetchrow('''SELECT * FROM modlogs WHERE guildid = $1;''', channel.guild.id) is None:
            pass
        else:
            if await self.bot.pool.fetchval(f'''SELECT chnneldelete FROM modlogs WHERE guildid = $1;''', channel.guild.id) is '0':
                query = '''SELECT modlogchan FROM modlogs WHERE guildid = $1'''
                row = await self.bot.pool.fetchrow(query, channel.guild.id)  
                channels = self.bot.get_channel(row['modlogchan'])
                await channels.send(
f'''
**Channel Delete Log**
`{channel.name}`(`{channel.id}`) category **{channel.category}**
''')
            else:
                pass

    async def on_guild_channel_update(self, before, after):
        if await self.bot.pool.fetchrow('''SELECT * FROM modlogs WHERE guildid = $1;''', before.guild.id) is None:
            pass
        else:
            if await self.bot.pool.fetchval(f'''SELECT chnnelupdate FROM modlogs WHERE guildid = $1;''', before.guild.id) is '0':
                query = '''SELECT modlogchan FROM modlogs WHERE guildid = $1'''
                row = await self.bot.pool.fetchrow(query, before.guild.id)  
                channel = self.bot.get_channel(row['modlogchan'])
                await channel.send(
f'''
**Channel Update Log**
`{channel.id}`
Before: **{before.name}** category **{before.category}**
After: **{after.name}** category **{after.category}**
''')
            else:
                pass

    async def on_member_join(self, member):
        if await self.bot.pool.fetchrow('''SELECT * FROM modlogs WHERE guildid = $1;''', member.guild.id) is None:
            pass
        else:
            if await self.bot.pool.fetchval(f'''SELECT memjoin FROM modlogs WHERE guildid = $1;''', member.guild.id) is '0':
                query = '''SELECT modlogchan FROM modlogs WHERE guildid = $1'''
                row = await self.bot.pool.fetchrow(query, member.guild.id)  
                channel = self.bot.get_channel(row['modlogchan'])
                await channel.send(
f'''
**New member!**
`{member.name}`(`{member.id}`)
Details
Discrim: **{member.discriminator}**
Avatar: **{member.avatar_url}**
Bot: **{member.bot}**
Account Created: **{member.created_at.__format__('%A, %d %B %Y')}**
''')
            else:
                pass

    async def on_member_remove(self, member):
        if await self.bot.pool.fetchrow('''SELECT * FROM modlogs WHERE guildid = $1;''', member.guild.id) is None:
            pass
        else:
            if await self.bot.pool.fetchval(f'''SELECT memleave FROM modlogs WHERE guildid = $1;''', member.guild.id) is '0':
                query = '''SELECT modlogchan FROM modlogs WHERE guildid = $1'''
                row = await self.bot.pool.fetchrow(query, member.guild.id)  
                channel = self.bot.get_channel(row['modlogchan'])
                await channel.send(
f'''
**Member left**
`{member.name}`(`{member.id}`)
```
''')
            else:
                pass

    async def on_guild_role_create(self, role):
        if await self.bot.pool.fetchrow('''SELECT * FROM modlogs WHERE guildid = $1;''', role.guild.id) is None:
            pass
        else:
            if await self.bot.pool.fetchval(f'''SELECT rolecreate FROM modlogs WHERE guildid = $1;''', role.guild.id) is '0':
                query = '''SELECT modlogchan FROM modlogs WHERE guildid = $1'''
                row = await self.bot.pool.fetchrow(query, role.guild.id)  
                channel = self.bot.get_channel(row['modlogchan'])
                await channel.send(
f'''
**Role Create Log**
`{role.name}`(`{role.id}`) color `{role.color}`
''')
            else:
                pass

    async def on_guild_role_delete(self, role):
        if await self.bot.pool.fetchrow('''SELECT * FROM modlogs WHERE guildid = $1;''', role.guild.id) is None:
            pass
        else:
            if await self.bot.pool.fetchval(f'''SELECT roledelete FROM modlogs WHERE guildid = $1;''', role.guild.id) is '0':
                query = '''SELECT modlogchan FROM modlogs WHERE guildid = $1'''
                row = await self.bot.pool.fetchrow(query, role.guild.id)  
                channel = self.bot.get_channel(row['modlogchan'])
                await channel.send(
f'''
**Role Delete Log**
`{role.name}`(`{role.id}`)
''')
            else:
                pass

    async def on_member_ban(self, guild, user):
        if await self.bot.pool.fetchrow('''SELECT * FROM modlogs WHERE guildid = $1;''', guild.id) is None:
            pass
        else:
            if await self.bot.pool.fetchval(f'''SELECT memban FROM modlogs WHERE guildid = $1;''', guild.id) is '0':
                query = '''SELECT modlogchan FROM modlogs WHERE guildid = $1'''
                row = await self.bot.pool.fetchrow(query, guild.id)  
                channel = self.bot.get_channel(row['modlogchan'])
                await channel.send(
f'''
**Member Ban Log**
`{user.name}`(`{user.id}`)
''')
            else:
                pass

    async def on_member_unban(self, guild, user):
        if await self.bot.pool.fetchrow('''SELECT * FROM modlogs WHERE guildid = $1;''', guild.id) is None:
            pass
        else:
            if await self.bot.pool.fetchval(f'''SELECT memunban FROM modlogs WHERE guildid = $1;''', guild.id) is '0':
                query = '''SELECT modlogchan FROM modlogs WHERE guildid = $1'''
                row = await self.bot.pool.fetchrow(query, guild.id)  
                channel = self.bot.get_channel(row['modlogchan'])
                await channel.send(
f'''
**Member Unban Log**
`{user.name}`(`{user.id}`)
''')
            else:
                pass

    #async def on_guild_role_update(self, before, after):
        #if await self.bot.pool.fetchrow('''SELECT * FROM modlogs WHERE guildid = $1;''', before.guild.id) is None:
            #pass
        #else:
            #if await self.bot.pool.fetchval(f'''SELECT roleupdate FROM modlogs WHERE guildid = $1;''', before.guild.id) is '0':
                #query = '''SELECT modlogchan FROM modlogs WHERE guildid = $1'''
                #row = await self.bot.pool.fetchrow(query, before.guild.id)  
                #channel = self.bot.get_channel(row['modlogchan'])
                #await channel.send(
#f'''
#Role Update Log
#`{before.id}``
#Before: **{before.name}** color **{before.color}**
#After: **{after.name}** color **{after.color}**
#''')
            #else:
                #pass

def setup(bot):
    bot.add_cog(modlogs(bot))