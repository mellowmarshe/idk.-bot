import discord
from discord.ext import commands
from discord.ext.commands import cooldown
from discord.ext.commands.cooldowns import BucketType
import asyncio
from datetime import datetime
import time
import random
import aiohttp
import json
from math import *
from utils import extras

class fun:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.guild_only()
    async def pat(self, ctx, *, user: discord.Member=None):
        embed = discord.Embed(color=random.choice(extras.EMBED_COLORS))
        if user is None or user is ctx.author:
            embed.set_image(url='https://media.giphy.com/media/mn1cym1jiJOUg/giphy.gif')
            await ctx.send(embed=embed)
            await ctx.send('`Wow... patting yourself...`')
        else:
            embed.set_image(url='https://media.giphy.com/media/L2z7dnOduqEow/giphy.gif')
            await ctx.send(embed=embed)
            await ctx.send(f'''`{random.choice(extras.PAT_RESPONSES)}`'''.replace('$receiver', user.name).replace('$giver', ctx.author.name))

    @commands.command()
    @commands.guild_only()
    async def hug(self, ctx, *, user: discord.Member=None):
        embed = discord.Embed(color=random.choice(extras.EMBED_COLORS))
        if user is None or user is ctx.author:
            embed.set_image(url='https://media.giphy.com/media/od5H3PmEG5EVq/giphy.gif')
            await ctx.send(embed=embed)
            await ctx.send('''`We all hug ourselves from time to time... right?!?`''')
        else:
            embed.set_image(url='https://media.giphy.com/media/od5H3PmEG5EVq/giphy.gif')
            await ctx.send(embed=embed)
            await ctx.send(f'''`{random.choice(extras.HUG_RESPONSES)}`'''.replace('$receiver', user.name).replace('$giver', ctx.author.name))

    @commands.command()
    @commands.guild_only()
    async def poke(self, ctx, *, user: discord.Member=None):
        embed = discord.Embed(color=random.choice(extras.EMBED_COLORS))
        if user is None or user is ctx.author:
            await ctx.send('`Poking yourself huh...`')
        else:
            embed.set_image(url='https://media.giphy.com/media/aZSMD7CpgU4Za/giphy.gif')
            await ctx.send(embed=embed)
            await ctx.send(f'''`{random.choice(extras.POKE_RESPONSES)}`'''.replace('$receiver', user.name).replace('$giver', ctx.author.name))

    @commands.command()
    @commands.guild_only()
    async def aesthetic(self, ctx, *, words=None):
        if words is None:
            words = ctx.author.name
        if len(words) >= 100:
            await ctx.send('You can not aesthetic more then 100 charcters at a time.')
        else:
            text = words.upper()
            await ctx.send(' '.join((extras.FANCY_CHARS.get(charin, charin) for charin in text)))

    @commands.command()
    @commands.guild_only()
    async def avatar(self, ctx, *, user: discord.Member=None):
        if user is None:
            user = ctx.author
        await ctx.send(f'''**{user.name}** avatar: {user.avatar_url_as(static_format='png', size=1024)}''')

    @commands.command()
    @commands.guild_only()
    async def clap(self, ctx, *, message: commands.clean_content(fix_channel_mentions=False, use_nicknames=True, escape_markdown=True)=None):
        if message is None:
            message = 'No text specified to clapify!'
        if len(message) >= 100:
            await ctx.send('You can not clapify more then 100 charcters at a time.')
        else:
            await ctx.send(f'{message}'.replace(' ', ' :clap: '))

    @commands.command()
    @commands.guild_only()
    async def reverse(self, ctx, *, message: commands.clean_content(fix_channel_mentions=False, use_nicknames=True, escape_markdown=True)=None):
        if message is None:
            message = 'No text specified to reverse!'
        if len(message) >= 100:
            await ctx.send('You can not reverse more then 100 charcters at a time.')
        await ctx.send(f''.join(reversed(message)))

    @commands.command()
    @commands.guild_only()
    async def wonky(self, ctx, *, message: commands.clean_content(fix_channel_mentions=False, use_nicknames=True, escape_markdown=True)=None):
        if message is None:
            message = 'Wonky huh'
        if len(message) >= 100:
            await ctx.send('You can not wonkify more then 100 charcters at a time.')
        await ctx.send(f'{message}'.swapcase())

    @commands.command()
    @commands.guild_only()
    async def echo(self, ctx, *, message: commands.clean_content()):
        if message is None:
            message = ctx.author.name
        await ctx.send(message)

    @commands.command()
    @commands.guild_only()
    async def osu(self, ctx, *, user):
        try:
            async with aiohttp.ClientSession() as cs:
                async with cs.get(f'''https://osu.ppy.sh/api/get_user?u={user}&m=0&k=9625112006e65a4d386edaf540fe2db35f7310f8''') as r:
                    res = await r.json()
                    embed = discord.Embed(color=0x000000)
                    embed.set_author(name=f'{user}s osu! stats')
                    embed.add_field(name='Level', value=int(float(res[0]['level'])))
                    embed.add_field(name='Accuracy', value=f'''{int(float(res[0]['accuracy'])):0.2f}%''')
                    embed.add_field(name='Country', value=res[0]['country'])
                    embed.add_field(name='Country Rank', value=res[0]['pp_country_rank'])
                    embed.add_field(name='Playcount', value=res[0]['playcount'])
                    embed.add_field(name='PP', value=int(float(res[0]['pp_raw'])))
                    embed.add_field(name='PP Country Rank', value=int(float(res[0]['pp_country_rank'])))
                    embed.add_field(name='Total Score', value=int(float(res[0]['total_score'])))
                    embed.add_field(name='Ranked Score', value=int(float(res[0]['ranked_score'])))
                    embed.set_footer(text='Powered by osu.ppy.sh', icon_url='https://cdn.discordapp.com/emojis/473116918333833220.png')
                    await ctx.send(embed=embed)
        except:
            await ctx.send(f'''**{user}** is not a valid osu! member. Make sure you typed it correctly.''')

    @commands.command()
    @commands.guild_only()
    async def cowsay(self, ctx, *, text: commands.clean_content(fix_channel_mentions=False, use_nicknames=True, escape_markdown=True)=None):
        if text is None:
            text = ctx.author.name
        if len(text) >= 20:
            await ctx.send('You can not make the cow say more then 20 charcters at a time.')             
        else:
            await ctx.send(
f'''
```
 _____________________ 
< {text} >
 --------------------- 
        \   ^__^
         \  (oo)\_______
            (__)\       )\/                
                ||----w |
                ||     ||
```
''')

    @commands.command()
    @commands.guild_only()
    async def linkit(self, ctx, messageID=None):
        if messageID is None:
            messageID = ctx.message.id 
        if len(messageID) != 18:
            await ctx.send('Improper message ID provided.')
        else:
            await ctx.send(f'''https://discordapp.com/channels/{ctx.guild.id}/{ctx.channel.id}/{messageID}''')

def setup(bot):
    bot.add_cog(fun(bot))