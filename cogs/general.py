import discord
from discord.ext import commands
from discord.ext.commands import cooldown
from discord.ext.commands.cooldowns import BucketType
import asyncio
from datetime import datetime
import time
from math import *
import psutil

class general():
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.guild_only()
    async def ping(self, ctx):
        t_1 = time.perf_counter()
        await ctx.trigger_typing() 
        t_2 = time.perf_counter()
        time_delta = round((t_2-t_1)*1000)
        await ctx.send(f':ping_pong: Pong! My websocket latency is **{round(self.bot.latency * 1000, 2)}**ms. Time to trigger typing is **{time_delta}**ms.')

    @commands.command(aliases=['upvote'])
    @commands.guild_only()
    async def vote(self, ctx):
            await ctx.send(
'''
Thanks for being interested in upvoting the bot :heart:. I greatly appreciate it. 
Upvote the bot here: https://discordbots.org/bot/476570085582962718/vote
''')

    @commands.command(aliases=['server'])
    @commands.guild_only()
    async def support(self, ctx):
            await ctx.send(
'''
If you need help with the bot or just want to join and chat you can join the support server.
Invite: https://discord.gg/sedt4tY
''')

def setup(bot):
	bot.add_cog(general(bot))