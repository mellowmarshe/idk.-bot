import discord
from discord.ext import commands
from discord.ext.commands import cooldown
from discord.ext.commands.cooldowns import BucketType
import asyncio
import asyncpg
import time
import aiofiles
from datetime import datetime
try:
    import uvloop
except ImportError:
    pass
else:
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

description = """idk. Bot."""

async def get_pre(bot, message):
    prefixes = await bot.pool.fetch('SELECT serverprefix FROM guildprefix WHERE guildid = $1', message.guild.id)
    if not prefixes:
        return commands.when_mentioned_or('!~')(bot, message)
    return commands.when_mentioned_or(*map(lambda row: row['serverprefix'], prefixes))(bot, message)

async def create_pool():
    credentials = {"user": "USER", "password": "PASSWORD", "database": "DATABASE", "host": "127.0.0.1"}
    pool = await asyncpg.create_pool(**credentials, max_size=100)
    return pool

bot = commands.Bot(command_prefix=get_pre, case_insensitive=True, description=description)
bot.launch_time = datetime.utcnow()
bot.remove_command('help')

startup_extensions = ['cogs.customprefixes', 'utils.errorhandler', 'jishaku', 'cogs.general', 'cogs.mcommands', 'cogs.modlogs', 'cogs.rolemanagement', 'cogs.fun', 'cogs.DBL', 'cogs.help', 'cogs.music']

@bot.event
async def on_ready():
    print("idk. bot is ready!")
    pool = await create_pool()
    bot.pool = pool
    await bot.change_presence(activity=discord.Streaming(name=f'in my database.', url='https://www.twitch.tv/mellowmarshe'))
    async with aiofiles.open('utils/schema.sql') as f:
        await bot.pool.execute(await f.read())

@bot.event
async def on_command_completion(ctx):
    await bot.pool.execute("UPDATE cmdcount SET cmdsused = cmdsused + 1;")

@bot.event
async def on_message(message):
    if not message.author.bot:
        await bot.process_commands(message)

@bot.event
async def on_guild_leave(guild):
    await bot.pool.execute('''DELETE FROM guildprefix WHERE guildid = $1;''', guild.id)
    await bot.pool.execute('''DELETE FROM modlogs WHERE guildid = $1;''', guild.id)

@bot.command(hidden=True)
async def load(ctx, extension_name : str):
    """Loads an extension."""
    if ctx.message.author.id == 300088143422685185:
        try:
            bot.load_extension(extension_name)
        except (AttributeError, ImportError) as e:
            await ctx.send("```py\n{}: {}\n```".format(type(e).__name__, str(e)))
            return
        await ctx.send("{} loaded.".format(extension_name))
    else:
        embed = discord.Embed(color=0x00FFFF, title=' ', description=
f'''
You may not use this command only the bot owner can.
''')
        await ctx.send(embed=embed)   

@bot.command(hidden=True)
async def unload(ctx, extension_name : str):
    """Unloads an extension."""
    if ctx.message.author.id == 300088143422685185:
        bot.unload_extension(extension_name)
        await ctx.send("{} unloaded.".format(extension_name))

    if __name__ == "__main__":
        for extension in startup_extensions:
            try:
                bot.load_extension(extension)
            except Exception as e:
                exc = '{}: {}'.format(type(e).__name__, e)
                print('Failed to load extension {}\n{}'.format(extension, exc))
    else:
        embed = discord.Embed(color=0x00FFFF, title=' ', description=
f'''
You may not use this command only the bot owner can.
''')
        await ctx.send(embed=embed)   

@bot.command(hidden=True)
async def die(ctx):
    if ctx.message.author.id == 300088143422685185:
        await ctx.send('Bye cruel world...')
        await bot.pool.close()
        await bot.logout()
    else:
        embed = discord.Embed(color=0x00FFFF, title=' ', description=
f'''
You may not use this command only the bot owner can.
''')
        await ctx.send(embed=embed) 

if __name__ == "__main__":
    for extension in startup_extensions:
        try:
            bot.load_extension(extension)
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            print('Failed to load extension {}\n{}'.format(extension, exc))

@bot.command(name='reload', hidden=True)
async def cog_reload(ctx, *, cog: str):
    """Command which Reloads a Module.
    Remember to use dot path. e.g: cogs.owner"""
    if ctx.message.author.id == 300088143422685185:
        bot.unload_extension(cog)
        bot.load_extension(cog)
        await ctx.send(f'**Successfuly reloaded {cog}**')
    else:
        embed = discord.Embed(color=0x00FFFF, title=' ', description=
    f'''
You may not use this command only the bot owner can.
    ''')
        await ctx.send(embed=embed) 

@bot.command()
@commands.guild_only()
async def uptime(ctx):
    if ctx.author.bot is False:
        delta_uptime = datetime.utcnow() - bot.launch_time
        hours, remainder = divmod(int(delta_uptime.total_seconds()), 3600)
        minutes, seconds = divmod(remainder, 60)
        days, hours = divmod(hours, 24)
        await ctx.send(f'''I have been up for **{days}**d **{hours}**h **{minutes}**m **{seconds}**s!''')
    else:
        pass

bot.run('TOKEN')